/*
  Copyright (c) 2020 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Kotlin Coroutines_

  https://commonsware.com/Coroutines
*/

package com.commonsware.coroutines.misc

import android.os.SystemClock
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.jraska.livedata.test
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class LiveDataExamplesTest {
  @get:Rule
  val instantTaskExecutorRule = InstantTaskExecutorRule()

  @Test
  fun liveDataFromSuspend() {
    val underTest = LiveDataExamples()

    val testObserver = underTest.liveSuspend().test()

    SystemClock.sleep(2100)

    testObserver.assertValue { it > 0 }
  }

  @Test
  fun liveDataFromFlow() {
    val underTest = LiveDataExamples()

    val testObserver = underTest.liveFlow().test()

    SystemClock.sleep(1600)

    testObserver.assertValueHistory(16, 77, 55)
  }
}
