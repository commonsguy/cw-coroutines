package com.commonsware.coroutines.misc

import androidx.test.ext.junit.runners.AndroidJUnit4
import kotlinx.coroutines.GlobalScope
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

private const val TEST_VALUE = "foo"

@RunWith(AndroidJUnit4::class)
class AsyncTaskAltTest {
  @Test
  fun asyncTaskTest() {
    var result: String? = null
    val latch = CountDownLatch(1)

    asyncTask({ TEST_VALUE }) {
      result = it
      latch.countDown()
    }

    latch.await(1, TimeUnit.SECONDS)

    assertEquals(TEST_VALUE, result)
  }

  @Test
  fun asyncTaskAltTest() {
    var result: String? = null
    val latch = CountDownLatch(1)

    GlobalScope.asyncTaskAlt({ TEST_VALUE }) {
      result = it
      latch.countDown()
    }

    latch.await(1, TimeUnit.SECONDS)

    assertEquals(TEST_VALUE, result)
  }
}
