/*
  Copyright (c) 2020-2021 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Kotlin Coroutines_

  https://commonsware.com/Coroutines
*/

package com.commonsware.coroutines.location

import android.content.pm.PackageManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import com.commonsware.coroutines.location.databinding.ActivityMainBinding
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import org.koin.androidx.viewmodel.ext.android.viewModel

private const val REQUEST_PERMS = 1337

class MainActivity : AppCompatActivity() {
  private val motor: MainMotor by viewModel()

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    val binding = ActivityMainBinding.inflate(layoutInflater)

    setContentView(binding.root)

    motor.results.onEach { state ->
      when (state) {
        MainMotor.ViewState.Loading -> {
          binding.loading.isVisible = true
          binding.content.isVisible = false
        }
        is MainMotor.ViewState.Content -> {
          binding.loading.isVisible = false
          binding.content.isVisible = true
          binding.content.text =
            getString(
              R.string.location,
              state.location.latitude,
              state.location.longitude
            )
        }
        MainMotor.ViewState.Error -> {
          binding.loading.isVisible = false
          binding.content.isVisible = true
          binding.content.text = getString(R.string.error_message)
        }
      }
    }
      .launchIn(lifecycleScope)

    motor.permissions
      .onEach { requestPermissions(it.toTypedArray(), REQUEST_PERMS) }
      .launchIn(lifecycleScope)

    if (savedInstanceState == null) motor.loadLocation()
  }

  override fun onRequestPermissionsResult(
    requestCode: Int,
    permissions: Array<out String>,
    grantResults: IntArray
  ) {
    if (requestCode == REQUEST_PERMS) {
      if (grantResults.all { it == PackageManager.PERMISSION_GRANTED }) {
        motor.loadLocation()
      } else {
        finish()
      }
    } else {
      super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }
  }
}
